export const state = () => ({
  sidebar: false,
  fiis_posse: []
})

export const mutations = {
  toggleSidebar (state) {
    state.sidebar = !state.sidebar
  }
}
